package com.softconsis.microservicio.app.contrato.models.service;

import java.util.List;

import com.softconsis.microservicio.app.contrato.models.entity.Contrato;

public interface IContratoService {

	public List<Contrato> findAll();
	public Contrato findById(Long id, int sueldo);
}
