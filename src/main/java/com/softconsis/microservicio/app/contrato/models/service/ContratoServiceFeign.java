package com.softconsis.microservicio.app.contrato.models.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softconsis.microservicio.app.contrato.client.EmpleadoClienteRest;
import com.softconsis.microservicio.app.contrato.models.entity.Contrato;

@Service("ContratoServiceF")
public class ContratoServiceFeign implements IContratoService {
	
	@Autowired
	private EmpleadoClienteRest empleadoFeign;

	@Override
	public List<Contrato> findAll() {
		
		return empleadoFeign.listar().stream().map(p -> new Contrato(p, 15)).collect(Collectors.toList());
	}

	@Override
	public Contrato findById(Long id, int sueldo) {
		
		return new Contrato(empleadoFeign.detalle(id), sueldo);
	}

}
