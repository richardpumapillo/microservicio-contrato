package com.softconsis.microservicio.app.contrato.models.entity;

import com.softconsis.microservicio.app.commons.models.entity.Empleado;

public class Contrato {

	private Empleado empleado;
	private int sueldo;

	public Contrato() {
	}

	public Contrato(Empleado empleado, int sueldo) {
		super();
		this.empleado = empleado;
		this.sueldo = sueldo;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public int getSueldo() {
		return sueldo;
	}

	public void setSueldo(int sueldo) {
		this.sueldo = sueldo;
	}
	
	
	

}
