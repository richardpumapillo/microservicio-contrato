package com.softconsis.microservicio.app.contrato.models.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.softconsis.microservicio.app.commons.models.entity.Empleado;
import com.softconsis.microservicio.app.contrato.models.entity.Contrato;

@Service("ContratoServiceRestTemplate")
public class ContratoServiceImpl implements IContratoService {
	
	@Autowired
	private RestTemplate clienteRest;

	@Override
	public List<Contrato> findAll() {
		List<Empleado> empleados = Arrays.asList(clienteRest.getForObject("http://microservicio-personal/api/empleado/listar", Empleado[].class));
		
		return empleados.stream().map(p -> new Contrato(p, 15)).collect(Collectors.toList());
	}

	@Override
	public Contrato findById(Long id, int sueldo) {
		
		Map<String, String> pathVariables = new HashMap<String, String>();
		pathVariables.put("id", id.toString());
		
		Empleado empleado = clienteRest.getForObject("http://microservicio-personal/api/empleado/ver/{id}", Empleado.class,pathVariables);
		
		
		return new Contrato(empleado, sueldo);
	}

}
