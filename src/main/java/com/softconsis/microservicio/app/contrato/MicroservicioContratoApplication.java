package com.softconsis.microservicio.app.contrato;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class MicroservicioContratoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicioContratoApplication.class, args);
	}

}
