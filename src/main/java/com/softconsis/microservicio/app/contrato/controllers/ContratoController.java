package com.softconsis.microservicio.app.contrato.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.softconsis.microservicio.app.contrato.models.entity.Contrato;
import com.softconsis.microservicio.app.contrato.models.service.IContratoService;

//@RequestMapping("/api/contrato")
@RefreshScope
@RestController
public class ContratoController {
	
	@Autowired
	@Qualifier("ContratoServiceF")
	private IContratoService contratoService;
	
	@GetMapping("/listar")
	public List<Contrato> listar(){
		return contratoService.findAll();
	}
	
	@GetMapping("/ver/{id}/sueldo/{sueldo}")
	public Contrato detalle(@PathVariable Long id, @PathVariable int sueldo) {
		
		return contratoService.findById(id, sueldo);
	}

}
