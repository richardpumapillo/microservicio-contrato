package com.softconsis.microservicio.app.contrato.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.softconsis.microservicio.app.commons.models.entity.Empleado;

@FeignClient(name = "microservicio-personal")
public interface EmpleadoClienteRest {
	
	@GetMapping(value = "/listar")
	public List<Empleado> listar();
	
	@GetMapping("/ver/{id}")
    public Empleado detalle(@PathVariable Long id);
}
