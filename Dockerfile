FROM openjdk:11
VOLUME /tmp
EXPOSE 8002
ADD ./target/microservicio-contrato-0.0.1-SNAPSHOT.jar microservicio-contrato.jar
ENTRYPOINT ["java","-jar","/microservicio-contrato.jar"]